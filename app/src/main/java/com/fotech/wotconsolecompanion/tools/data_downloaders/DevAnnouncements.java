package com.fotech.wotconsolecompanion.tools.data_downloaders;

/**
 * Download messages from my directory on EECS server
 */
public class DevAnnouncements extends AbstractDataDownloader {

    public DevAnnouncements() {
        super("https://wot-console-companion-api.herokuapp.com/news");
    }

}
