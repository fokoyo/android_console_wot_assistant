package com.fotech.wotconsolecompanion.tools.async_workers;

import android.app.Activity;
import android.os.AsyncTask;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.ViewGroup;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.tools.data_downloaders.DevAnnouncements;
import com.fotech.wotconsolecompanion.main.layout_builders.DevMessageBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * communicate with the wcc api web application and gather any available announcements
 */
public class WorkerAnnouncements extends AsyncTask<Void, Void, Void> {

    private WeakReference<Activity> activity;

    public WorkerAnnouncements(Activity activity) {
        this.activity = new WeakReference<>(activity);
    }


    @Override
    protected Void doInBackground(Void... voids) {
        displayMessages();

        postExecute();
        return null;
    }

    /**
     * post execution to avoid runtime exception
     */
    private void postExecute(){
        activity.get().runOnUiThread(()->{
            SwipeRefreshLayout srl = activity.get().findViewById(R.id.refresh_container);
            srl.setRefreshing(false);
        });
    }


    /**
     * @return messages from my server (website)
     */
    private JSONArray downloadMessages() {


        try {
            DevAnnouncements downloader = new DevAnnouncements();
            JSONObject data = new JSONObject(downloader.download());

            return data.getJSONArray("messages");

        } catch (Exception e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }

    /**
     * convert messages into views and display them on screen
     */
    private void displayMessages() {

        final JSONArray messages = downloadMessages();

        final ViewGroup messageContainer = activity.get().findViewById(R.id.messages);

        activity.get().runOnUiThread(messageContainer::removeAllViews);

        for (int i = 0; i < messages.length(); i++) {

            final int finalI = i;
            activity.get().runOnUiThread(() -> {
                try {
                    DevMessageBuilder builder = new DevMessageBuilder(messages.getJSONObject(finalI), activity.get());

                    messageContainer.addView(builder.buildLayout());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
