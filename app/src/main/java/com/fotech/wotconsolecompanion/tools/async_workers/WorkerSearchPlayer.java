package com.fotech.wotconsolecompanion.tools.async_workers;

import android.app.Activity;
import android.os.AsyncTask;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.SparseIntArray;
import android.view.WindowManager;
import android.widget.Toast;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.main.fragment_helpers.PlayerDetailsHelper;
import com.fotech.wotconsolecompanion.tools.data_cachers.PlayerSearchHistoryCacher;
import com.fotech.wotconsolecompanion.utilities.StaticData;
import com.fotech.wotconsolecompanion.utilities.Util;
import com.fotech.wotconsolecompanion.main.fragments.PlayerSearchFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * search for a player and return information regarding said player.
 */
public class WorkerSearchPlayer extends AsyncTask<String, Void, Void> {

    private WeakReference<Activity> activity;
    private PlayerSearchFragment fragment;

    public WorkerSearchPlayer(Activity activity) {
        this(activity, null);
    }

    public WorkerSearchPlayer(Activity activity, PlayerSearchFragment fragment) {

        this.activity = new WeakReference<>(activity);
        this.fragment = fragment;
    }

    /**
     * Deactivate the screen to prevent the user from clicking search multiple times in a row.
     */
    @Override
    protected void onPreExecute() {


        activity.get()
                .getWindow()
                .setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        SwipeRefreshLayout refreshLayout = activity.get().findViewById(R.id.refresh_container);
        refreshLayout.setRefreshing(true);

        /* make sure the previous players information is gone*/
        StaticData.clearPlayerData();

    }

    /**
     * gather the necessary information
     *
     * @param params parameters for the search
     * @return player's information
     */
    @Override
    protected Void doInBackground(String... params) {

        String platform = params[0];
        String name = params[1];

        if (name.isEmpty()) {
            postExecute(
                    errorEmpty()
            );
            return null;
        }

        postExecute(
                getPlayerInformation(platform, name)
        );
        return null;
    }


    /**
     * Show player details
     *
     * @param result players data
     */
    private void postExecute(String result) {

        activity.get().runOnUiThread(() -> {
            activity.get()
                    .getWindow()
                    .clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            SwipeRefreshLayout refreshLayout = activity.get().findViewById(R.id.refresh_container);
            refreshLayout.setRefreshing(false);

            try {


                /*Show appropriate toast message*/
                if (result.toLowerCase().contains("error")) {
                    JSONObject jsonResult = new JSONObject(result);
                    String errorMessage = jsonResult.getString("error");

                    Toast.makeText(this.activity.get(),
                            errorMessage,
                            Toast.LENGTH_SHORT).show();
                } else { /*Send user to player details activity (page)*/

                    StaticData.setPlayerData(new JSONObject(result));

                    new PlayerDetailsHelper(activity.get(),StaticData.getPlayerData());

                }

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this.activity.get(),
                        "Service unavailable",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }


    /**
     * Collects the account id from WG's API and calls getPlayerData in
     * order to get the data of that player. The return value from getPlayerData is
     * the return value for request player.
     * - if the player was not found, the stats are never requested.
     *
     * @param platform XBox or ps4
     * @param name     name of the player to search for
     * @return Player's data in JSON formatted string
     */
    private String getPlayerInformation(String platform, String name) {
        String url;
        int id;

        url = String.format(
                Util.PLAYER_LINK_FORMAT,
                platform,
                "&search=" + name
        );


        try {
            String response = Util.doHttpRequest(url); //get id from wot api

            if (response.contains("error")) {
                return playerNotFound();
            }

            id = new JSONObject(response)
                    .getJSONArray("data")
                    .getJSONObject(0)
                    .getInt("account_id");

        } catch (Exception e) {
            e.printStackTrace();
            return encounteredProblem();
        }


        return getPlayerData(platform, id + "");
    }

    /**
     * Get a json representation of all the player's data
     *
     * @param platform xBox or ps4
     * @param playerId the player's id
     * @return players data
     */
    private String getPlayerData(String platform, String playerId) {

        try {
                                                                                                                                                // statistics.max_frags,statistics.max_frags_tank_id,statistics.max_damage,statistics.max_damage_tank_id
            String query = "&account_id=" + playerId + "&fields=account_id,nickname,created_at,last_battle_time,updated_at,statistics.all,statistics.trees_cut,statistics.max_frags,statistics.max_frags_tank_id,statistics.max_damage,statistics.max_damage_tank_id";
            String playerDataLink = String.format(
                    Util.PLAYER_STATS_LINK_FORMAT,
                    platform,
                    query
            );


            query = "&account_id=" + playerId + "&extra=clan&fields=joined_at,role,clan";
            String clanDataLink = String.format(
                    Util.PLAYER_CLAN_FORMAT,
                    platform,
                    query
            );


            String playerDataResponse = Util.doHttpRequest(playerDataLink);
            String clanDataResponse = Util.doHttpRequest(clanDataLink);


            JSONObject clanMembershipInformation = getClanMembershipInformation(playerId, new JSONObject(clanDataResponse));


            JSONObject playerData = new JSONObject(playerDataResponse)
                    .getJSONObject("data")
                    .getJSONObject(playerId);

            addPlayerToHistory(playerData.getString("nickname"));


            JSONObject allPlayerStatistics = getAllPlayerStatistics(platform, playerId, playerData);
            JSONObject formattedData = formatPlayerData(platform, playerData, allPlayerStatistics, clanMembershipInformation);

            return formattedData.toString();


        } catch (Exception e) {
            e.printStackTrace();
            return dataNotFound();
        }

    }

    /**
     * get all the players statistics from the API response
     *
     * @param platform   platform they play on
     * @param playerId   player's id
     * @param playerData the player's data
     * @return all their statistics as a json object
     * @throws JSONException if a problem is encountered
     */
    private JSONObject getAllPlayerStatistics(String platform, String playerId, JSONObject playerData) throws JSONException {

        String trees_cut = playerData
                .getJSONObject("statistics")
                .getString("trees_cut");

        int maxDmg = playerData.getJSONObject("statistics").getInt("max_damage");
        int maxDmgTankId = playerData.getJSONObject("statistics").getInt("max_damage_tank_id");

        int maxFrags = playerData.getJSONObject("statistics").getInt("max_frags");
        int maxFragsTankId = playerData.getJSONObject("statistics").getInt("max_frags_tank_id");

        return playerData
                .getJSONObject("statistics")
                .getJSONObject("all")
                .put("trees_cut", trees_cut)
                .put("avg_tier", calcAverageTier(platform, playerId))
                .put("max_damage", maxDmg)
                .put("max_damage_tank_id", maxDmgTankId)
                .put("max_frags", maxFrags)
                .put("max_frags_tank_id", maxFragsTankId);

    }

    /**
     * formats the players data to be easily acceded by application
     *
     * @param platform            platform they play on
     * @param playerData          players data from wargaming
     * @param allPlayerStatistics player's "all" statistics
     * @param playerClanInfo      players clan information (If they are in one)
     * @return formatted json
     * @throws JSONException iff an error occurs
     */
    private JSONObject formatPlayerData(String platform, JSONObject playerData, JSONObject allPlayerStatistics, JSONObject playerClanInfo) throws JSONException {
        JSONObject formattedData = new JSONObject();
        formattedData.put("platform", platform.toUpperCase())
                .put("nickname", playerData.getString("nickname"))
                .put("account_id", playerData.getInt("account_id"))
                .put("created_at", playerData.getInt("created_at"))
                .put("last_battle_time", playerData.getInt("last_battle_time"))
                .put("updated_at", playerData.getInt("updated_at"))
                .put("stats", allPlayerStatistics)
                .put("clan", playerClanInfo);

        return formattedData;
    }

    /**
     * get a players clan membership information
     *
     * @param playerId        player's id
     * @param clanInformation json containing clan information
     * @return players membership information or null if they do not belong to a clan
     */
    private JSONObject getClanMembershipInformation(String playerId, JSONObject clanInformation) {


        try {
            JSONObject clanData = clanInformation.getJSONObject("data").getJSONObject(playerId);

            JSONObject clanMembership = new JSONObject();
            clanMembership.put("clan_name", clanData.getJSONObject("clan").getString("name"))
                    .put("clan_tag", clanData.getJSONObject("clan").getString("tag"))
                    .put("role", clanData.getString("role"))
                    .put("joined", clanData.getLong("joined_at"));

            return clanMembership;
        } catch (Exception e) {
//            e.printStackTrace();
            return null;
        }

    }


    /**
     * Calculates a player's average tank tier. In order to reduce the amount
     * of work the app must do, A File containing A list of all the tanks in
     * the game is kept.
     * <p>
     * ALGORITHM: find all the tanks(tank_id) that a player has. Cross reference
     * the tank file in assets to get tank tiers. take the wighted
     * sum of all the tanks by tier(the ones that the player has).
     * <p>
     * If at any point, an exception is thrown, a 0 is returned.
     *
     * @param platform XBox or ps4
     * @param playerId PLayer's account Id
     * @return players average tank tier
     */
    private double calcAverageTier(String platform, String playerId) {

        try {

            String query = "&account_id=" + playerId +
                    "&fields=tank_id,all.battles,all.wins,all.damage_dealt,all.frags,all.spotted,all.dropped_capture_points";

            String playerTankLink = String.format(
                    Util.PLAYER_TANKS_FORMAT,
                    platform,
                    query
            );


            String response = Util.doHttpRequest(playerTankLink);

            JSONObject data = new JSONObject(response).getJSONObject("data");
            try {
                StaticData.setPlayerTanks(data.getJSONArray(playerId));
            } catch (Exception e) {
                e.printStackTrace();
                StaticData.setPlayerTanks(null);
            }
            JSONArray playerTanks = StaticData.getPlayerTanks();

            /* get player's tanks and battles in them*/

            SparseIntArray tankBattles = getPlayerBattlesInTanks(playerTanks);

            /* get tanks in the game and their tiers*/

            int[] battlesInEachTier = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            getBattlesInEachTier(battlesInEachTier, tankBattles);

            int totalBattles = 0, weightedSum = 0, tier = 1;

            for (int battlesInTier : battlesInEachTier) {
                totalBattles += battlesInTier;

                weightedSum += battlesInTier * tier;

                tier++;
            }

            double avgTier = (double) weightedSum / (double) totalBattles;
            double result = Util.formatDoubleAsDouble(avgTier);
            return Double.isNaN(result) ? 0 : result;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * get the nimbler of battles a player has played in each of their tanks
     *
     * @param playerTanks players tanks and respective data
     * @return array mapping of tank ids to number of battles played
     */
    private SparseIntArray getPlayerBattlesInTanks(JSONArray playerTanks) {

        SparseIntArray tankBattles = new SparseIntArray();
        int numTanks = playerTanks == null ? 0 : playerTanks.length();

        try {
            for (int i = 0; i < numTanks; i++) {
                JSONObject json = playerTanks.getJSONObject(i);
                int tankId = json.getInt("tank_id");
                int battles = json.getJSONObject("all").getInt("battles");

                tankBattles.put(tankId, battles);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tankBattles;
    }

    /**
     * get an array representing all the battles a player has had in each tier
     *
     * @param tankBattles player's tanks and battles in them
     */
    private void getBattlesInEachTier(int[] battlesInTier, SparseIntArray tankBattles) {

        int maxBattles = Integer.MIN_VALUE;
        int tankWithMostBattles = 0;

        for (int i = 0; i < tankBattles.size(); i++) {
            int tankId = tankBattles.keyAt(i);
            int tankTier = 1;
            int numOfBattlesInTank;

            try {
                tankTier = StaticData.getTankObject(tankId).getInt("tier");
                numOfBattlesInTank = tankBattles.get(tankId);

                if (numOfBattlesInTank > maxBattles) {
                    maxBattles = numOfBattlesInTank;
                    tankWithMostBattles = tankId;
                }

            } catch (Exception e) {
                e.printStackTrace();
                numOfBattlesInTank = 0;
            }
            battlesInTier[tankTier - 1] += numOfBattlesInTank;
        }

        StaticData.setPlayerFavoriteTank(tankWithMostBattles, maxBattles);

    }


    /**
     * add a player's name to the search his tory
     *
     * @param player name to be added
     */
    private void addPlayerToHistory(String player) {

        if (fragment == null)
            return;

        PlayerSearchHistoryCacher cacher = PlayerSearchHistoryCacher.getInstance();
        String cache = cacher.retrieve();

        JSONObject json = new JSONObject();
        try {
            if (cache == null) {

                JSONArray newHistory = new JSONArray();
                newHistory.put(player);
                json.put("history", newHistory);
                cacher.store(json.toString());
                activity.get().runOnUiThread(() -> fragment.updateSearchHistoryView());

            } else if (!cache.contains(player)) {

                JSONArray history = new JSONObject(cache).getJSONArray("history");
                history.put(player);
                json.put("history", history);
                cacher.store(json.toString());
                activity.get().runOnUiThread(() -> fragment.updateSearchHistoryView());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * @return data unavailable message for retrieving data
     */
    private String dataNotFound() {
        return "{\"error\":\"Data Unavailable\"}";
    }

    /**
     * @return player not specified message if player not specified
     */
    private String errorEmpty() {
        return "{\"error\":\"Enter player\"}";
    }

    /**
     * @return player not found message if data was not found
     */
    private String playerNotFound() {
        return "{\"error\":\"Player not found\"}";
    }

    /**
     * @return problem encountered
     */
    private String encounteredProblem() {
        return "{\"error\":\"problem encountered\"}";
    }

}
