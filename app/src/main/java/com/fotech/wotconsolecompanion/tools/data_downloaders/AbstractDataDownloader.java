package com.fotech.wotconsolecompanion.tools.data_downloaders;

import com.fotech.wotconsolecompanion.utilities.Util;


/**
 * responsible for gathering data from online api and formatting into a concise, easy to read json
 * object.
 */
public abstract class AbstractDataDownloader {

    /**
     * url to the location of the data
     */
    private String url;

    /**
     * initialize the url
     */
    AbstractDataDownloader(String url) {
        this.url = url;
    }

    /**
     * parent method that is inherited by all children of this class.
     *
     * performs an http request and parses the response into a string object. If an
     * exception occurs, null is returned
     *
     */
    public String download() {

        return Util.doHttpRequest(url);

    }

}
