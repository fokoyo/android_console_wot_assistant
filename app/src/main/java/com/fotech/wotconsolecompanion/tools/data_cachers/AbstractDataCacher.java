package com.fotech.wotconsolecompanion.tools.data_cachers;


import android.content.Context;
import android.content.SharedPreferences;

import com.fotech.wotconsolecompanion.utilities.StaticReferences;
import com.fotech.wotconsolecompanion.main.activities.MainActivity;

import java.util.Date;

/**
 * responsible for caching data that would need to be reused in the future
 */
abstract class AbstractDataCacher {

    private String preferenceName;
    private String dataKey;
    private long expiration;

    /**
     * @param preferenceName name of the shared preference
     * @param dataName       name og the data in the shared preference
     * @param expiration     time expended before cache is considered expired (milliseconds)
     */
    AbstractDataCacher(String preferenceName, String dataName, long expiration) {
        this.preferenceName = preferenceName;
        this.dataKey = dataName;
        this.expiration = expiration;
    }

    /**
     * store (cache) data
     *
     * @param data data to be cached
     */
    public void store(String data) {

        SharedPreferences preferences = getActivity()
                .getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Date date = new Date();

        editor.clear()
                .putString(dataKey, data)
                .putLong("time_stamp", date.getTime())
                .apply();

    }

    /**
     * get the data from the cache
     *
     * @return get data from the cache
     */
    public String retrieve() {

        SharedPreferences preferences = getActivity()
                .getSharedPreferences(preferenceName, Context.MODE_PRIVATE);

        return preferences.getString(dataKey, null);

    }

    /**
     * clear the cache if it exists
     */
    public void clear() {
        if (cacheExists()) {
            SharedPreferences preferences = getActivity()
                    .getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.clear()
                    .putStringSet(dataKey, null)
                    .apply();
        }
    }

    /**
     * check if there exists a cache of this data already
     *
     *
     * @return true if cache exists
     */
    public boolean cacheExists() {

        SharedPreferences preferences = getActivity()
                .getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        return preferences.contains(dataKey);

    }

    /**
     * check if cache has expired (if cache exists)
     *
     * @return true if cache has expired
     */
    public boolean cacheExpired() {

        SharedPreferences preferences = getActivity()
                .getSharedPreferences(preferenceName, Context.MODE_PRIVATE);

        long time_stamp = preferences.getLong("time_stamp", 0);
        long current_time = new Date().getTime();

        /*1 hour in milliseconds*/
        return Math.abs(current_time - time_stamp) > expiration;

    }

    /**
     * @return main activity
     */
    private MainActivity getActivity() {
        return StaticReferences.mainActivity.get();
    }
}
