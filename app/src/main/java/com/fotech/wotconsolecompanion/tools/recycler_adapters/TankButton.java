package com.fotech.wotconsolecompanion.tools.recycler_adapters;

import android.app.Activity;

import java.lang.ref.WeakReference;

public class TankButton {
    private int tankId;
    private String imageLink;
    private String tankName;
    private int flagId;
    private int typeId;
    private String tier;

    private WeakReference<Activity> activity;


    public TankButton(Activity activity, int tankId, String imageLink, String tankName, int flagId, int typeId, String tier) {

        this.activity = new WeakReference<>(activity);

        this.tankId = tankId;
        this.imageLink = imageLink;
        this.tankName = tankName;
        this.flagId = flagId;
        this.typeId = typeId;
        this.tier = tier;
    }

    public WeakReference<Activity> getActivity() {
        return activity;
    }

    int getTankId() {
        return tankId;
    }

    String getImageLink() {
        return imageLink;
    }

    String getTankName() {
        return tankName;
    }

    int getFlagId() {
        return flagId;
    }

    int getTypeId() {
        return typeId;
    }

    public String getTier() {
        return tier;
    }
}
