package com.fotech.wotconsolecompanion.tools.data_cachers;


public class PlayerSearchHistoryCacher extends AbstractDataCacher {


    private static PlayerSearchHistoryCacher instance;

    /**
     */
    private PlayerSearchHistoryCacher() {
        super("playerSearchHistory",
                "historyJson",
                0);
    }


    /**
     * @return singleton instance of class
     */
    public static PlayerSearchHistoryCacher getInstance() {
        if (instance == null)
            instance = new PlayerSearchHistoryCacher();

        return instance;
    }
}
