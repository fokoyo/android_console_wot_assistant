package com.fotech.wotconsolecompanion.tools.data_downloaders;

import com.fotech.wotconsolecompanion.beans.TechTree;
import com.fotech.wotconsolecompanion.utilities.StaticData;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainData extends AbstractDataDownloader {

    private String response;

    public MainData() {
        super("https://wot-console-companion-api.herokuapp.com/api/wn8");
    }

    /**
     * process data from cache
     *
     * @param data cached data
     */
    public static void processDataFromCache(String data) {
        try {

            /*convert general tank data to json*/
            JSONArray allTanks = new JSONObject(data).getJSONArray("rows");
            StaticData.setTechTree(new TechTree(allTanks));

            for (int i = 0; i < allTanks.length(); i++) {

                JSONObject currentTank = allTanks.getJSONObject(i);
                /*add tank to the mapping*/
                StaticData.addTank(currentTank.getInt("tid"), currentTank);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public String download() {
        response = super.download();
        return (response.isEmpty() || response.contains("\"err") || response.contains("\"success\":false")) ? null : response;
    }

    /**
     * process data and stores it in a static class to be used whenever
     */
    public void processData() {
        processDataFromCache(this.response);
    }
}
