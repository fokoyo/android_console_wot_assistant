package com.fotech.wotconsolecompanion.tools.async_workers;

import android.app.Activity;
import android.os.AsyncTask;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.View;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.tools.recycler_adapters.ClanTuple;
import com.fotech.wotconsolecompanion.utilities.Util;
import com.fotech.wotconsolecompanion.main.fragments.ClansFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;

/**
 * download clans from wcc database by interfacing with wcc api web application
 */
public class WorkerDownloadClans extends AsyncTask<Void, Void, Void> {

    private WeakReference<Activity> activity;
    private WeakReference<ClansFragment> clansFragment;
    private String platform;

    public WorkerDownloadClans(String platform, Activity activity, ClansFragment clansFragment) {
        this.activity = new WeakReference<>(activity);
        this.clansFragment = new WeakReference<>(clansFragment);
        this.platform = platform;
    }


    @Override
    protected Void doInBackground(Void... voids) {


        int pid = platform.equalsIgnoreCase("xbox") ? 1 : 2;

        String url = Util.HEROKU_API + "clans/?platform=%d";

        passResultsToFragment(Util.doHttpRequest(
                String.format(Locale.getDefault(), url, pid)
        ));

        postExecute();
        return null;
    }

    /**
     * prevent runtime exception with this
     */
    private void postExecute(){
        activity.get().runOnUiThread(()->{
            SwipeRefreshLayout srl = activity.get().findViewById(R.id.refresh_container);
            srl.setRefreshing(false);
        });
    }


    /**
     * load the data into the layout
     *
     * @param result result set from database query
     */
    private void passResultsToFragment(String result) {
        try {

            JSONArray rows = new JSONObject(result).getJSONArray("rows");

            if(rows.length() != 0){

                clansFragment.get().setAdapter(
                        new ClansFragment.Adapter(getItemList(rows))
                );

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Get a list of clan tuples
     *
     * @param resultSet result form wcc api
     * @return an list of clan tuple objects
     */
    private ArrayList<ClanTuple> getItemList(JSONArray resultSet) {

        ArrayList<ClanTuple> result = new ArrayList<>();

        try {
            for (int i = 0; i < resultSet.length(); i++) {
                JSONObject row = resultSet.getJSONObject(i);

                int cid = row.getInt("cid");
                String tag = "["+ row.getString("tag") + "]";
                int members = row.getInt("aplayers");
                int platform = row.getInt("platform");
                float winRate = (float) row.getDouble("awinrate") * 100;
                int wn8 = (int) Math.ceil(row.getDouble("wn8t15"));


                result.add(new ClanTuple(activity.get(), cid, tag, members, platform, winRate, wn8, i + 1));

            }
        } catch (Exception e) { e.printStackTrace(); }

        return result;
    }


}

