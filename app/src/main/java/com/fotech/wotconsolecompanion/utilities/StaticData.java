package com.fotech.wotconsolecompanion.utilities;

import android.util.SparseArray;

import com.fotech.wotconsolecompanion.beans.TechTree;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This is a class that contains data that is static to the entire application
 */
public class StaticData {

    /*Global Data
     * The intention of these attributes is to encapsulated data transference
     * between activities. For example, After the client has entered a valid
     * player name, THe following activity needs to know what the data for
     * that player is. Instead of passing the data directly to the activity,
     * We put the data in a static variable so that the following activity
     * has access to it. THE DATA MUST BE STORED BEFORE THE NEXT ACTIVITY IS
     * STARTED!*/


    /*
    keeps track of the information of a player being searched
     */
    private static JSONObject playerData = null;
    private static JSONArray playerTanks = null;
    private static JSONObject favoriteTank = null;

    /*
    keeps track of the selected clans
     */
    private static JSONObject selectedClan = null;

    /*
    keeps track of all the tanks in the game
     */
    private static SparseArray<JSONObject> tankDataMap = new SparseArray<>();
    private static TechTree techTree = null;

    /*
    keeps track of the id of the tank that a user has selected
     */
    private static int selectedTankId = 0;

    /**
     * get player data
     *
     * @return player data
     */
    public static JSONObject getPlayerData() {
        return playerData;
    }

    /**
     * set player data
     *
     * @param data player data
     */
    public static void setPlayerData(JSONObject data) {
        playerData = data;
    }

    /**
     * get player's tank data
     *
     * @return player's tank data
     */
    public static JSONArray getPlayerTanks() {
        return playerTanks;
    }

    /**
     * set player's tank data
     *
     * @param tanks player's tank data
     */
    public static void setPlayerTanks(JSONArray tanks) {
        playerTanks = tanks;
    }

    /**
     * set the values for the players favorite tank
     *
     * @param id      tank id
     * @param battles battles in tank
     */
    public static void setPlayerFavoriteTank(int id, int battles) {

        if (favoriteTank == null) {
            favoriteTank = new JSONObject();
        }

        try {
            favoriteTank.put("id", id).put("battles", battles);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * get favorite tank
     *
     * @return favorite tank
     */
    public static JSONObject getPlayerFavoriteTank() {
        return favoriteTank;
    }


    /**
     * get a map of all the tanks
     *
     * @return map of all tanks
     */
    static SparseArray<JSONObject> getAllTankData() {
        return tankDataMap;
    }

    /**
     * add a tank to the map
     *
     * @param tank_id tanks's id
     * @param tank    tank data
     */
    public static void addTank(int tank_id, JSONObject tank) {
        tankDataMap.put(tank_id, tank);
    }

    /**
     * get the json representation of a tank given it's id
     *
     * @param tank_id tank's id
     * @return tank json
     */
    public static JSONObject getTankObject(int tank_id) {

        return tankDataMap.get(tank_id);
    }

    /**
     * get the id of the tank that has been selected
     *
     * @return selected tank id
     */
    public static int getSelectedTankId() {
        return selectedTankId;
    }

    /**
     * when the user selects a tank, the tank id is stored in a static variable
     *
     * @param id tank id
     */
    public static void setSelectedTankId(int id) {
        selectedTankId = id;
    }

    /**
     * get the tank that was selected
     *
     * @return tank json
     */
    public static JSONObject getSelectedTankObject() {
        return tankDataMap.get(selectedTankId);
    }

    /**
     * get the tech tree object
     *
     * <code>TechTree</code> contains all the tanks in the game arranged in order of their
     * respective nations, types and tiers.
     *
     * @return tech tree object
     */
    public static TechTree getTechTree() {
        return techTree;
    }

    /**
     * set the tech tree object
     *
     * @param techTree tech tree object
     */
    public static void setTechTree(TechTree techTree) {
        StaticData.techTree = techTree;
    }

    /**
     * clears player's data from memory
     */
    public static void clearPlayerData() {
        playerData = null;
        playerTanks = null;
        favoriteTank = null;
    }


    /**
     *
     * @return the json object associated with the clan selected in the clans view.
     */
    public static JSONObject getSelectedClan() {
        return selectedClan;
    }

    /**
     *
     * @param selectedClan the json object associated with the clan selected in the clans view.
     */
    public static void setSelectedClan(JSONObject selectedClan) {
        StaticData.selectedClan = selectedClan;
    }
}
