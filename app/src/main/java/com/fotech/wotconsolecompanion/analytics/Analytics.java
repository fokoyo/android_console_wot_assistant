package com.fotech.wotconsolecompanion.analytics;

import android.app.Activity;

import com.fotech.wotconsolecompanion.utilities.Debug;
import com.koushikdutta.ion.Ion;

/**
 * class responsible for tracking usage of fragments and activities  for analytics.
 */
public class Analytics {

    public static int PLAYER_SEARCH = 1;
    public static int TANK_SEARCH = 2;
    public static int CLAN_SELECTED = 3;
    public static int MESSAGES = 4;


    /**
     * sends an post request to the wcc server with the id of the used feature
     * @param id feature id
     */
    public static void used(Activity activity, int id){

        Ion.with(activity)
                .load("http://wot-console-companion-api.herokuapp.com/analytics?id="+id)
                .asString()
                .setCallback((err,res) ->{

                    if(err != null){
                        err.printStackTrace();
                    }else{
                        Debug.log(res);
                    }
                });
    }



}
