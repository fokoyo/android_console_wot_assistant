package com.fotech.wotconsolecompanion.main.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.tools.async_workers.WorkerTankopedia;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;

public class TankViewActivity extends AbstractActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tank_view);
        setToolbarTitle("Tanks");

        configActionBar();

        loadTanks();

    }

    /**
     * configure the action bar
     */
    private void configActionBar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * load tanks into recycler view
     */
    private void loadTanks() {
        WorkerTankopedia worker = new WorkerTankopedia(this);
        worker.execute(StaticReferences.tpo.get());
    }


    /**
     * static method to that returns an intent to the caller.
     * encapsulates the intent method.
     *
     * @param context context of the caller
     * @return Intent
     */
    public static Intent getIntent(Context context) {

        return new Intent(context, TankViewActivity.class);
    }


}

