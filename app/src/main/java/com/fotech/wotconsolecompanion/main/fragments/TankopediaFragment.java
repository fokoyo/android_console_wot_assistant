package com.fotech.wotconsolecompanion.main.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridLayout;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.analytics.Analytics;
import com.fotech.wotconsolecompanion.tools.data_cachers.LastUsedFragmentCache;
import com.fotech.wotconsolecompanion.beans.TankopediaParameterObject;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;
import com.fotech.wotconsolecompanion.utilities.Util;
import com.fotech.wotconsolecompanion.main.activities.TankViewActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class TankopediaFragment extends AbstractFragment {

    private View view;
    private Activity activity;

    private final String TYPE = "type";
    private final String TIER = "tier";
    private final String NATION = "nation";
    private final String CLASS = "class";
    private Map<String, Set<Integer>> parameters;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setToolbarTitle("Tankopedia");

        disableRefresh();

        activity = getActivity();
        view = inflater.inflate(R.layout.fragment_tankopedia, container, false);


        parameters = new HashMap<>();
        parameters.put(TYPE, new TreeSet<>());
        parameters.put(TIER, new TreeSet<>());
        parameters.put(NATION, new TreeSet<>());
        parameters.put(CLASS, new TreeSet<>());

        configureFilterButtons();

        configureApplyButtons();

        parsePreviousFilter();

        return view;
    }


    @Override
    public void onStart() {

        LastUsedFragmentCache.getInstance().store(
                LastUsedFragmentCache.FragmentTypes.TANKOPEDIA_FILTER_FRAGMENT + ""
        );

        super.onStart();
    }


    /**
     * configure capability of filter buttons
     */
    private void configureFilterButtons() {

        ViewGroup nation = view.findViewById(R.id.nation);
        ViewGroup tier = view.findViewById(R.id.tier);
        ViewGroup type = view.findViewById(R.id.type);

        for (int i = 0; i < nation.getChildCount(); i++) {
            nation.getChildAt(i).setOnClickListener(this::nationClicked);
        }

        for (int i = 0; i < tier.getChildCount(); i++) {
            tier.getChildAt(i).setOnClickListener(this::tierClicked);
        }

        for (int i = 0; i < type.getChildCount(); i++) {
            type.getChildAt(i).setOnClickListener(this::typeClicked);
        }

        CheckBox premium = view.findViewById(R.id.premium);
        CheckBox elite = view.findViewById(R.id.elite);

        premium.setOnCheckedChangeListener((button,checked)-> classChecked(button.getId(),checked));
        elite.setOnCheckedChangeListener((button,checked)-> classChecked(button.getId(),checked));

        elite.setChecked(true);

    }

    /**
     * on click event for nation filter buttons
     *
     * @param v nation filter button
     */
    public void nationClicked(View v) {
        toggleSelected(NATION, v);
    }

    /**
     * on click event for tier filter buttons
     *
     * @param v tier filter button
     */
    public void tierClicked(View v) {
        toggleSelected(TIER, v);
    }

    /**
     * on click event for type filter buttons
     *
     * @param v type filter button
     */
    public void typeClicked(View v) {
        toggleSelected(TYPE, v);
    }


    /**
     * what happens when a class checkbox is clicked
     *
     * @param boxId id for the checkbox
     * @param checked is checked or not
     */
    public void classChecked(int boxId, boolean checked){

        int boxClass = boxId == R.id.premium ? 1 : 2;

        if(checked)
            Objects.requireNonNull(parameters.get(CLASS)).add(boxClass);
        else
            Objects.requireNonNull(parameters.get(CLASS)).remove(boxClass);

    }


    /**
     * toggle the tag attribute to indicated being selected or not
     *
     * @param v button being clicked
     */
    private void toggleSelected(String parameterKey, View v) {
        String tag = v.getTag().toString();

        if (tag.equalsIgnoreCase("0")) {
            v.setTag("1"); //selected
            Objects.requireNonNull(parameters.get(parameterKey))
                    .add(Integer.parseInt(v.getContentDescription().toString()));
        } else {
            v.setTag("0"); //de-selected
            Objects.requireNonNull(parameters.get(parameterKey))
                    .remove(Integer.parseInt(v.getContentDescription().toString()));
        }

        toggleHighlight(v);

    }

    /**
     * toggle the background color for highlighted/selected buttons
     *
     * @param v button that was selected
     */
    private void toggleHighlight(View v) {
        String tag = v.getTag().toString();

        if (tag.equalsIgnoreCase("1")) {
//            v.setBackgroundColor(activity.getColor(R.color.orange)); //selected
            v.setBackground(activity.getDrawable(R.drawable.container_background_highlighted));
        } else {
//            v.setBackgroundColor(activity.getColor(R.color.grey)); // de-selected
            v.setBackground(activity.getDrawable(R.drawable.container_background_dark));
        }
    }

    /**
     * configure the cancel and apply buttons in the filter container
     */
    private void configureApplyButtons() {

        Button applyButton = view.findViewById(R.id.apply_button);
        applyButton.setOnClickListener(v -> {
            /*apply selections and filter tanks
             * then return to previous activity*/

            submit();

        });


        Button clearButton = view.findViewById(R.id.clear_button);
        clearButton.setOnClickListener(v -> {
            /*clear all the selected buttons*/

            GridLayout nation = view.findViewById(R.id.nation);
            GridLayout tier = view.findViewById(R.id.tier);
            GridLayout type = view.findViewById(R.id.type);


            clearSelection(NATION, nation);
            clearSelection(TIER, tier);
            clearSelection(TYPE, type);
            clearCheckBoxes();


        });
    }

    /**
     * clear the selection of all the views in the given linear layout
     *
     * @param group linear layout containing children to be cleared
     */
    private void clearSelection(String parameterKey, ViewGroup group) {

        int count = group.getChildCount();

        for (int i = 0; i < count; i++) {
            View child = group.getChildAt(i);

            child.setTag("0");
//            child.setBackgroundColor(activity.getColor(R.color.grey));
            child.setBackground(activity.getDrawable(R.drawable.container_background_dark));

            Objects.requireNonNull(parameters.get(parameterKey))
                    .remove(Integer.parseInt(child.getContentDescription().toString()));
        }

    }

    /**
     * clear checkboxes for premium and elite tanks
     */
    private void clearCheckBoxes(){
        CheckBox premium = activity.findViewById(R.id.premium);
        CheckBox elite = activity.findViewById(R.id.elite);

        premium.setChecked(false);
        elite.setChecked(false);
    }

    /**
     * submit parameters to and asynchronous task to filter tanks
     */
    private void submit() {

        Analytics.used(activity, Analytics.TANK_SEARCH);

        cacheCurrentFilter();

        StaticReferences.tpo = new WeakReference<>(new TankopediaParameterObject(parameters));

        Intent intent = TankViewActivity.getIntent(activity);
        activity.startActivity(intent);

    }

    /**
     * get the previous parameters used to filter the last set of tanks
     */
    private void parsePreviousFilter() {

        SharedPreferences preferences = activity.getSharedPreferences("filter", Context.MODE_PRIVATE);
        String parameterJson = preferences.getString("parameterJson", null);

        if (parameterJson == null) {
            /*use default filter*/
            toggleSelected(NATION, view.findViewById(R.id.filter_nation_1));
            toggleSelected(TIER, view.findViewById(R.id.filter_tier_10));

            JSONObject data = new JSONObject();

            try {
                data.put("nations", new JSONArray().put(1))
                        .put("tiers", new JSONArray().put(10))
                        .put("types", new JSONArray());


            } catch (Exception e) {
                e.printStackTrace();
            }

            SharedPreferences.Editor editor = preferences.edit();
            editor.clear()
                    .putString("parameterJson", data.toString())
                    .apply();
        } else {
            /*parse previous filter*/
            try {
                JSONObject data = new JSONObject(parameterJson);

                JSONArray nations = data.getJSONArray("nations");
                JSONArray tiers = data.getJSONArray("tiers");
                JSONArray types = data.getJSONArray("types");


                for (int i = 0; i < nations.length(); i++) {
                    int id = Util.getViewByName(activity, "filter_nation_" + nations.getString(i));
                    toggleSelected(NATION, view.findViewById(id));
                }

                for (int i = 0; i < tiers.length(); i++) {
                    int id = Util.getViewByName(activity, "filter_tier_" + tiers.getString(i));
                    toggleSelected(TIER, view.findViewById(id));
                }

                for (int i = 0; i < types.length(); i++) {
                    int id = Util.getViewByName(activity, "filter_type_" + types.getString(i));
                    toggleSelected(TYPE, view.findViewById(id));
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * cache the parameters of the current filter
     */
    private void cacheCurrentFilter() {

        SharedPreferences preferences = activity.getSharedPreferences("filter", Context.MODE_PRIVATE);

        JSONArray nations = new JSONArray();
        JSONArray tiers = new JSONArray();
        JSONArray types = new JSONArray();


        addSelectedFilter(view.findViewById(R.id.nation), nations);

        addSelectedFilter(view.findViewById(R.id.tier), tiers);

        addSelectedFilter(view.findViewById(R.id.type), types);

        JSONObject data = new JSONObject();

        try {
            data.put("nations", nations)
                    .put("tiers", tiers)
                    .put("types", types);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SharedPreferences.Editor editor = preferences.edit();

        editor.clear()
                .putString("parameterJson", data.toString())
                .apply();
    }

    /**
     * add a parameter to the array to be cached
     *
     * @param buttonContainer container of buttons
     * @param array           json array to add parameter to
     */
    private void addSelectedFilter(ViewGroup buttonContainer, JSONArray array) {

        for (int i = 0; i < buttonContainer.getChildCount(); i++) {
            String tag = buttonContainer.getChildAt(i).getTag().toString();
            String desc = buttonContainer.getChildAt(i).getContentDescription().toString();

            if (tag.equalsIgnoreCase("1")) {
                array.put(desc);
            }
        }

    }
}
