package com.fotech.wotconsolecompanion.main.layout_builders;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * build a view that  displays the player bases performance with a given tank
 */

public class PlayerBasePerformanceBuilder extends AbstractLayoutBuilder {

    /**
     *
     * @param data data being assigned
     * @param activity activity building the layout
     */
    public PlayerBasePerformanceBuilder(JSONObject data, Activity activity) {
        super(data,activity);

    }

    @Override
    View inflateLayout(WeakReference<Activity> context) {
        return View.inflate(context.get(), R.layout.layout_tank_player_performance, null);
    }

    @Override
    void assignData(JSONObject data, View inflatedLayout) {

        try {

            String winRate = "Win Rate: " + data.getString("winRate");
            String dmg = "Expected Damage: " + data.getString("dmg");
            String frags = "Kills per game: " + data.getString("frags");
            String spots = "Spots per game: " + data.getString("spots");
            String def = "Defence points per game: " + data.getString("def");


            TextView winRateView = inflatedLayout.findViewById(R.id.winrate);
            TextView expDmgView = inflatedLayout.findViewById(R.id.dmg);
            TextView kpgView = inflatedLayout.findViewById(R.id.kpg);
            TextView spgView = inflatedLayout.findViewById(R.id.spg);
            TextView dpgView = inflatedLayout.findViewById(R.id.dpg);

            winRateView.setText(winRate);
            expDmgView.setText(dmg);
            kpgView.setText(frags);
            spgView.setText(spots);
            dpgView.setText(def);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
