package com.fotech.wotconsolecompanion.main.layout_builders;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class TankOriginsBuilder extends AbstractLayoutBuilder {

    private String details;

    public TankOriginsBuilder(String details, Activity activity) {
        super(null,activity);

        this.details = details;
    }

    @Override
    View inflateLayout(WeakReference<Activity> context) {
        return View.inflate(context.get(), R.layout.layout_tank_origins, null);
    }

    @Override
    void assignData(JSONObject data, View inflatedLayout) {

        TextView detailsView = inflatedLayout.findViewById(R.id.details);

        detailsView.setText(details);
    }
}
