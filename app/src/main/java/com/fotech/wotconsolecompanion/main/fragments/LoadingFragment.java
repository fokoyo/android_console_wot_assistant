package com.fotech.wotconsolecompanion.main.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fotech.wotconsolecompanion.R;

public class LoadingFragment extends AbstractFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setToolbarTitle(" ");

        disableRefresh();

        return inflater.inflate(R.layout.fragment_loading, container, false);
    }
}
