package com.fotech.wotconsolecompanion.main.layout_builders;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;

import org.json.JSONObject;

import java.lang.ref.WeakReference;


public class DevMessageBuilder extends AbstractLayoutBuilder {

    /**
     *
     * @param data data being assigned
     * @param activity activity using the builder
     */
    public DevMessageBuilder(JSONObject data, Activity activity) {
        super(data,activity);

    }

    @Override
    View inflateLayout(WeakReference<Activity> context) {
        return View.inflate(context.get(), R.layout.layout_dev_message, null);
    }

    @Override
    void assignData(JSONObject data, View inflatedLayout) {

        try {
            String title = data.getString("title");
            String message = data.getString("message");
            String date = data.getString("date");

            TextView titleView = inflatedLayout.findViewById(R.id.dev_title);
            TextView messageView = inflatedLayout.findViewById(R.id.dev_message);
            TextView dateView = inflatedLayout.findViewById(R.id.dev_date);

            titleView.setText(title);
            messageView.setText(message);
            dateView.setText(date);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
