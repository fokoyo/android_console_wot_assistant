package com.fotech.wotconsolecompanion.main.activities;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;

public abstract class AbstractActivity extends AppCompatActivity {


    /**
     * hide keyboard
     *
     * @param activity activity using the method
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void setToolbarTitle(String text) {

        TextView title = this.findViewById(R.id.toolbar_title);
        title.setText(text);

    }

    /**
     * Computes the color classification for the efficiency metric
     *
     * @param wn8 WN8
     * @return Color Bracket for that WN8
     */
    public int getWn8Color(double wn8) {

        if (wn8 >= 0 && wn8 <= 299)
            return getColor(R.color.vry_bad);
        if (wn8 > 299 && wn8 <= 449)
            return getColor(R.color.bad);
        if (wn8 > 449 && wn8 <= 649)
            return getColor(R.color.blw_avg);
        if (wn8 > 649 && wn8 <= 899)
            return getColor(R.color.avg);
        if (wn8 > 899 && wn8 <= 1199)
            return getColor(R.color.abv_avg);
        if (wn8 > 1199 && wn8 <= 1599)
            return getColor(R.color.good);
        if (wn8 > 1599 && wn8 <= 1999)
            return getColor(R.color.vry_good);
        if (wn8 > 1999 && wn8 <= 2449)
            return getColor(R.color.great);
        if (wn8 > 2449 && wn8 <= 2899)
            return getColor(R.color.uni);
        if (wn8 > 2899)
            return getColor(R.color.superuni);

        return getColor(R.color.leftover_eff);
    }

    /**
     * Computes the color classification for the efficiency metric
     *
     * @param wn7 WN7
     * @return Color Bracket for that WN7
     */
    public int getWn7Color(double wn7) {

        if (wn7 >= 0 && wn7 <= 449)
            return getColor(R.color.vry_bad);
        if (wn7 > 499 && wn7 <= 699)
            return getColor(R.color.bad);
        if (wn7 > 699 && wn7 <= 899)
            return getColor(R.color.blw_avg);
        if (wn7 > 899 && wn7 <= 1099)
            return getColor(R.color.avg);
        if (wn7 > 1099 && wn7 <= 1349)
            return getColor(R.color.good);
        if (wn7 > 1349 && wn7 <= 1549)
            return getColor(R.color.vry_good);
        if (wn7 > 1549 && wn7 <= 1849)
            return getColor(R.color.great);
        if (wn7 > 1849 && wn7 <= 2049)
            return getColor(R.color.uni);
        if (wn7 > 2049)
            return getColor(R.color.superuni);

        return getColor(R.color.leftover_eff);
    }

    /**
     * Computes the color classification for the efficiency metric
     *
     * @param eff EFF
     * @return Color Bracket for that EFF
     */
    public int getEffColor(double eff) {

        if (eff >= 0 && eff <= 629)
            return getColor(R.color.bad);
        if (eff > 629 && eff <= 859)
            return getColor(R.color.blw_avg);
        if (eff > 859 && eff <= 1139)
            return getColor(R.color.avg);
        if (eff > 1139 && eff <= 1459)
            return getColor(R.color.good);
        if (eff > 1459 && eff <= 1734)
            return getColor(R.color.great);
        if (eff > 1734)
            return getColor(R.color.uni);

        return getColor(R.color.leftover_eff);

    }


    /**
     *
     * @param winRate winrate
     * @return color bracket for that winrate
     */
    public int getWinRateColor(double winRate){

        if(winRate < 46)
            return getColor(R.color.vry_bad);
        if(winRate >= 46 && winRate < 47)
            return getColor(R.color.bad);
        if(winRate >= 47 && winRate < 48)
            return getColor(R.color.blw_avg);
        if(winRate >= 48 && winRate < 50)
            return getColor(R.color.avg);
        if(winRate >= 50 && winRate < 52)
            return getColor(R.color.abv_avg);
        if(winRate >= 52 && winRate < 54)
            return getColor(R.color.good);
        if(winRate >= 54 && winRate < 56)
            return getColor(R.color.vry_good);
        if(winRate >= 56 && winRate < 60)
            return getColor(R.color.great);
        if(winRate >= 60 && winRate < 65)
            return getColor(R.color.uni);
        else
            return getColor(R.color.superuni);

    }


    /**
     * Computes the name classification for the efficiency metric
     *
     * @param wn8 WN8
     * @return name Bracket for that WN8
     */
    public String getWn8String(double wn8) {

        if (wn8 >= 0 && wn8 <= 299)
            return "Very Bad";
        if (wn8 >= 300 && wn8 <= 449)
            return "Bad";
        if (wn8 >= 450 && wn8 <= 649)
            return "Below Average";
        if (wn8 >= 650 && wn8 <= 899)
            return "Average";
        if (wn8 >= 900 && wn8 <= 1199)
            return "Above Average";
        if (wn8 >= 1200 && wn8 <= 1599)
            return "Good";
        if (wn8 >= 1600 && wn8 <= 1999)
            return "Very Good";
        if (wn8 >= 2000 && wn8 <= 2449)
            return "Great";
        if (wn8 >= 2450 && wn8 <= 2899)
            return "Unicum";
        if (wn8 >= 2900)
            return "Super Unicum";

        return "Null";
    }

    /**
     * Computes the name classification for the efficiency metric
     *
     * @param wn7 WN7
     * @return name Bracket for that WN7
     */
    public String getWn7String(double wn7) {

        if (wn7 >= 0 && wn7 <= 449)
            return "Very Bad";
        if (wn7 >= 500 && wn7 <= 699)
            return "Bad";
        if (wn7 >= 700 && wn7 <= 899)
            return "Below Average";
        if (wn7 >= 900 && wn7 <= 1099)
            return "Average";
        if (wn7 >= 1100 && wn7 <= 1349)
            return "Good";
        if (wn7 >= 1350 && wn7 <= 1549)
            return "Very Good";
        if (wn7 >= 1550 && wn7 <= 1849)
            return "Great";
        if (wn7 >= 1850 && wn7 <= 2049)
            return "Unicum";
        if (wn7 >= 2050)
            return "Super Unicum";

        return "Null";
    }

    /**
     * Computes the name classification for the efficiency metric
     *
     * @param eff EFF
     * @return name Bracket for that EFF
     */
    public String getEffString(double eff) {

        if (eff >= 0 && eff <= 629)
            return "Bad";
        if (eff >= 630 && eff <= 859)
            return "Below Average";
        if (eff >= 860 && eff <= 1139)
            return "Average";
        if (eff >= 1140 && eff <= 1459)
            return "Good";
        if (eff >= 1460 && eff <= 1734)
            return "Great";
        if (eff >= 1735)
            return "Unicum";

        return "Null";

    }

}
