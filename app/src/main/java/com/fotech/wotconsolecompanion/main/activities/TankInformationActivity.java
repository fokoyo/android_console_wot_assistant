package com.fotech.wotconsolecompanion.main.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.main.layout_builders.PlayerBasePerformanceBuilder;
import com.fotech.wotconsolecompanion.main.layout_builders.TankHeaderBuilder;
import com.fotech.wotconsolecompanion.main.layout_builders.TankOriginsBuilder;
import com.fotech.wotconsolecompanion.main.layout_builders.TankPackagesBuilder;
import com.fotech.wotconsolecompanion.utilities.StaticData;
import com.fotech.wotconsolecompanion.utilities.Util;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;

/**
 * refactor this class
 */
public class TankInformationActivity extends AbstractActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tank_information);

        setToolbarTitle("Information");

        configActionBar();

        loadTankHeader();
        loadTankOrigins();
        loadPlayerBaseStatistics();
        downloadTankPackages();

    }


    /**
     * configure the action bar
     */
    private void configActionBar() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * static method to that returns an intent to the caller.
     * encapsulates the intent method.
     *
     * @param context context of the caller
     * @return Intent
     */
    public static Intent getIntent(Context context) {

        return new Intent(context, TankInformationActivity.class);
    }

    /**
     * load the header containing basic tank information
     */
    private void loadTankHeader() {

        JSONObject tank = StaticData.getSelectedTankObject();


        ViewGroup container = findViewById(R.id.tank_information_container);

        TankHeaderBuilder builder = new TankHeaderBuilder(tank, this);
        View layout = builder.buildLayout();
        container.addView(layout, 0);

        int tier = Integer.parseInt(
                layout.findViewById(R.id.tier).getContentDescription().toString()
        );
        if (tier < 5) {
            findViewById(R.id.marks).setVisibility(View.GONE);
        } else {
            findViewById(R.id.marks).setVisibility(View.VISIBLE);
            loadMOE(tank);
        }
    }

    /**
     * load MOE values for specified tank
     *
     * @param tank specified tank
     */
    private void loadMOE(JSONObject tank) {

        try {

            TextView moe1 = findViewById(R.id.moe1);
            TextView moe2 = findViewById(R.id.moe2);
            TextView moe3 = findViewById(R.id.moe3);


            //----- new code -----

            Ion.with(this)
                    .load(Util.HEROKU_API + "moe?id=" + tank.getInt("tid"))
                    .asString()
                    .setCallback((err, result) -> {
                        if (err != null) {
                            err.printStackTrace();
                        } else {

                            try {

                                JSONObject data = new JSONObject(result).getJSONArray("rows").getJSONObject(0);

                                moe1.setText(String.format(
                                        Locale.getDefault(),
                                        " %d",
                                        data.getInt("moe1")
                                ));

                                moe2.setText(String.format(
                                        Locale.getDefault(),
                                        " %d",
                                        data.getInt("moe2")
                                ));

                                moe3.setText(String.format(
                                        Locale.getDefault(),
                                        " %d",
                                        data.getInt("moe3")
                                ));


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    });


            //--------------------

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Load the tanks origins as described by wargaming
     */
    private void loadTankOrigins() {
        ViewGroup parent = findViewById(R.id.tank_information_container);

        final View originsView = new TankOriginsBuilder("", TankInformationActivity.this).buildLayout();
        originsView.findViewById(R.id.clan_search_progress).setVisibility(View.VISIBLE);
        parent.addView(originsView, 1);


        String query = "&tank_id=" + StaticData.getSelectedTankId() + "&fields=description";

        String link = String.format(
                Util.WG_TANK_FORMAT,
                query
        );


        Ion.with(this)
                .load(link)
                .asString()
                .setCallback((e, result) -> {
                    // do stuff with the result or error
                    if (e != null) {
                        e.printStackTrace();
                    } else {

                        JSONObject json = new JSONObject();
                        try {
                            json = new JSONObject(result);
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                        String details = null;
                        try {
                            details = json.getJSONObject("data")
                                    .getJSONObject(StaticData.getSelectedTankId() + "")
                                    .getString("description");
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                        TextView detailsView = originsView.findViewById(R.id.details);
                        detailsView.setText(details);

                        originsView.findViewById(R.id.clan_search_progress).setVisibility(View.GONE);
                    }
                });

    }

    /**
     * load the tank's player base statistics
     */
    private void loadPlayerBaseStatistics() {


        try {

            // ----- new code --------

            JSONObject data = StaticData.getSelectedTankObject();
            JSONObject result = new JSONObject();

            double winRate = data.getDouble("winrate") * 100;
            int dmg = data.getInt("dmg");
            double frags = data.getDouble("frag");
            double spots = data.getDouble("spot");
            double def = data.getDouble("def");

            result.put("winRate", Util.formatDouble(winRate) + "%");
            result.put("dmg", dmg);
            result.put("frags", Util.formatDouble(frags));
            result.put("spots", Util.formatDouble(spots));
            result.put("def", Util.formatDouble(def));

            ViewGroup parent = findViewById(R.id.tank_information_container);
            PlayerBasePerformanceBuilder builder = new PlayerBasePerformanceBuilder(result, TankInformationActivity.this);
            parent.addView(builder.buildLayout(), 2);

            // -----------------------


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * download the tanks packages from WG's api
     */
    private void downloadTankPackages() {

        String query = "&tank_id=" + StaticData.getSelectedTankId();

        String link = String.format(
                Util.WG_TANK_PACKAGE_FORMAT,
                query
        );

        Ion.with(this)
                .load(link)
                .asString()
                .setCallback((e, result) -> {
                    // do stuff with the result or error
                    if (e != null) {
                        e.printStackTrace();
                    } else {

                        JSONObject json = new JSONObject();
                        try {
                            json = new JSONObject(result);
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        processPackageData(json);
                    }
                });
    }


    /**
     * helper method for loading tank packages
     *
     * @param data json object from http request
     */
    private void processPackageData(JSONObject data) {


        try {///////////******&&&&&&%%%%%%$$$$$$
            data = data.getJSONObject("data").getJSONObject(StaticData.getSelectedTankId() + "");

            JSONObject packages = data.getJSONObject("packages");
            JSONObject packagesTree = data.getJSONObject("packages_tree");

            /*
            form a list of package ids in chronological order
            */
            TreeSet<Integer> packageStructure = getPkgIdList(packagesTree);

            /*
            create a scroll view containing package names
            */
            TankPackagesBuilder builder = new TankPackagesBuilder(
                    this, packageStructure, packagesTree, packages
            );


            ViewGroup parent = findViewById(R.id.tank_information_container);
            parent.addView(builder.buildLayout(), 3);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * return a list of the package id's in chronological order
     *
     * @param json package tree
     * @return list of packages in chronological order
     */
    private TreeSet<Integer> getPkgIdList(JSONObject json) {
        TreeSet<Integer> set = new TreeSet<>();


        Iterator<String> keyIterator = json.keys();

        while (keyIterator.hasNext()) {
            set.add(Integer.parseInt(keyIterator.next()));
        }


        return set;
    }


}
