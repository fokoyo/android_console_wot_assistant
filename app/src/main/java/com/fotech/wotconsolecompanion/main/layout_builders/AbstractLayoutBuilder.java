package com.fotech.wotconsolecompanion.main.layout_builders;

import android.app.Activity;
import android.view.View;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * responsible for building views by tanking layout files and inflating
 * them into view objects. These object would then be filled with information and
 * displayed on the screen.
 */
public abstract class AbstractLayoutBuilder {

    private JSONObject data;
    WeakReference<Activity> activity;

    /**
     *
     * @param data data to fill layout with
     * @param activity activity using builder
     */
    AbstractLayoutBuilder(JSONObject data, Activity activity) {
        this.data = data;
        this.activity = new WeakReference<>(activity);
    }

    /**
     *
     * @return a build layout containing all necessary information.
     */
    public View buildLayout(){
        View result = inflateLayout(activity);

        assignData(data,result);

        return  result;
    }

    /**
     * Layout varies depending on the concrete layout builder
     *
     * @return inflated layout to be filled with data.
     */
    abstract View inflateLayout(WeakReference<Activity> context);


    /**
     * assign data to the layout
     *
     * @param data data to fill layout with
     * @param inflatedLayout layout that was inflated
     */
    abstract void assignData(JSONObject data, View inflatedLayout);

}
