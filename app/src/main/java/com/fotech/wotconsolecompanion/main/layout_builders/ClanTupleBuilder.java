package com.fotech.wotconsolecompanion.main.layout_builders;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.analytics.Analytics;
import com.fotech.wotconsolecompanion.utilities.Debug;
import com.fotech.wotconsolecompanion.utilities.StaticData;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;
import com.fotech.wotconsolecompanion.utilities.Util;
import com.fotech.wotconsolecompanion.main.activities.ClanViewActivity;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Locale;

/**
 * build a tuple containing a single clan's information. This tuple is then placed
 * in a table by another class
 */
public class ClanTupleBuilder extends AbstractLayoutBuilder {


    /**
     * @param data     data to fill layout with
     * @param activity activity using builder (clans fragment)
     */
    public ClanTupleBuilder(JSONObject data, Activity activity) {
        super(data, activity);
    }

    @Override
    View inflateLayout(WeakReference<Activity> context) {
        return View.inflate(context.get(), R.layout.layout_clan_tuple, null);
    }

    @Override
    void assignData(JSONObject data, View inflatedLayout) {

        inflatedLayout.setOnClickListener(v -> {

            Analytics.used(activity.get(), Analytics.CLAN_SELECTED);

            StaticData.setSelectedClan(data);

            Intent intent = ClanViewActivity.getIntent(activity.get());
            activity.get().startActivity(intent);
        });

        try {
            TextView number = inflatedLayout.findViewById(R.id.number);
            TextView tag = inflatedLayout.findViewById(R.id.tag);
            TextView members = inflatedLayout.findViewById(R.id.members);
            TextView winRate = inflatedLayout.findViewById(R.id.winRate);
            TextView wn8 = inflatedLayout.findViewById(R.id.wn8);

            if (data.getBoolean("even"))
                inflatedLayout.setBackground(activity.get().getDrawable(R.drawable.background_table_row1));
            else
                inflatedLayout.setBackground(activity.get().getDrawable(R.drawable.background_table_row2));

            number.setText(String.format(Locale.getDefault(), "%s", data.getInt("number")));

            tag.setText(data.getString("tag"));

            members.setText(String.format(Locale.getDefault(),"%d", data.getInt("members")));

            winRate.setText(String.format(Locale.getDefault(), "%.1f%%", data.getDouble("winRate")));
            winRate.setBackgroundColor(
                    StaticReferences.mainActivity.get().getWinRateColor(
                            data.getDouble("winRate")
                    )
            );

            wn8.setText(String.format(Locale.getDefault(), "%d", data.getInt("wn8")));
            wn8.setBackgroundColor(
                    StaticReferences.mainActivity.get().getWn8Color(
                            data.getInt("wn8")
                    )
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
