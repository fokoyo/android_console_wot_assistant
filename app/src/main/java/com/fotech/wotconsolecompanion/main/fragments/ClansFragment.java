package com.fotech.wotconsolecompanion.main.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.main.layout_builders.ClanTupleBuilder;
import com.fotech.wotconsolecompanion.tools.async_workers.WorkerDownloadClans;
import com.fotech.wotconsolecompanion.tools.data_cachers.LastUsedFragmentCache;
import com.fotech.wotconsolecompanion.tools.recycler_adapters.ClanTuple;
import com.fotech.wotconsolecompanion.utilities.StaticReferences;
import com.fotech.wotconsolecompanion.utilities.Util;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * This fragment is responsible for displaying clans and giving the user the opportunity
 * to add clans that they want to see on the list.
 */
public class ClansFragment extends AbstractFragment implements SwipeRefreshLayout.OnRefreshListener {

    //-----------------------------------------------------------------------------------
    /**
     * adapter class responsible for controlling table pages
     */
    public static class Adapter {

        private int currentPage;
        private int numberOfPages;
        private int itemsPerPage;
        private int numberOfItems;
        private ArrayList<ClanTuple> itemList;

        public Adapter(ArrayList<ClanTuple> itemList) {
            this.currentPage = 1;
            this.itemsPerPage = Util.MAX_CLANS_PER_PAGE;
            this.numberOfItems = itemList.size();

            this.numberOfPages = (int) Math.ceil((double) this.numberOfItems / this.itemsPerPage);

            this.itemList = itemList;
        }

        /**
         *
         * @return all results from search
         */
        ArrayList<ClanTuple> getAllPages(){
            return this.itemList;
        }

        /**
         * @return current page
         */
        List<ClanTuple> getCurrentPage() {
            int[] indexes = getSubIndexes(currentPage);

            return itemList.subList(indexes[0], indexes[1]);
        }

        /**
         *
         * @return previous page
         */
        List<ClanTuple> getPrevPage() {

            currentPage--;
            if (currentPage <= 0) currentPage = 1;

            int[] indexes = getSubIndexes(currentPage);

            return itemList.subList(indexes[0], indexes[1]);
        }


        /**
         * @return next page
         */
        List<ClanTuple> getNextPage() {

            currentPage++;
            if (currentPage > numberOfPages) currentPage = numberOfPages;

            int[] indexes = getSubIndexes(currentPage);

            try {
                return itemList.subList(indexes[0], indexes[1]);
            }catch (Exception e){
                e.printStackTrace();
                return new ArrayList<>();
            }


        }


        /**
         * @param  page page number
         * @return starting and ending indexes of a page
         */
        private int[] getSubIndexes(int page) {
            int maxIndex = numberOfItems - 1;

            int startIndex = (page * itemsPerPage) - itemsPerPage;
            int endIndex = startIndex + itemsPerPage;

            if (endIndex > maxIndex) {
                int itemsRemaining = maxIndex - startIndex;
                endIndex = startIndex + itemsRemaining + 1;
            }

            return new int[]{startIndex, endIndex};
        }


        /**
         * @return true if user is on first page
         */
        boolean isFirstPage() {
            return currentPage == 1;
        }

        /**
         * @return true if user is on last page
         */
        boolean isLastPage() {
            return currentPage == numberOfPages;
        }

        /**
         * @return current page
         */
        int getCurrentPageNumber() {
            return currentPage;
        }

        /**
         * @return number of pages
         */
        int getNumberOfPages() {
            return numberOfPages;
        }
    }
    //-----------------------------------------------------------------------------------

    private ClansFragment.Adapter adapter;

    private ArrayList<ClanTuple> clans;

    private View view;
    private Activity activity;

    private SwipeRefreshLayout srl;

    private String platform = "xbox";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setToolbarTitle("Clans (prototype)");
        view = inflater.inflate(R.layout.fragment_clans, container, false);
        activity = getActivity();

        assert activity != null;
        srl = activity.findViewById(R.id.refresh_container);
        srl.setColorSchemeColors(activity.getColor(R.color.orange),
                activity.getColor(R.color.ice_blue),
                activity.getColor(R.color.grey),
                activity.getColor(R.color.supreme));
        srl.setOnRefreshListener(this);

        enableRefresh();


        configTableLayout();
        configureSwitch();
        configureSearch();

        onRefresh(); // refresh layout

        StaticReferences.clansFragment = new WeakReference<>(this);

        return view;
    }


    @Override
    public void onStart() {

        LastUsedFragmentCache.getInstance().store(
                LastUsedFragmentCache.FragmentTypes.CLANS_FRAGMENT + ""
        );

        super.onStart();
    }

    /**
     * set the table adapter
     * @param adapter adapter to be set
     */
    public void setAdapter(Adapter adapter) {
        this.adapter = adapter;

        if(clans == null) //sets the list once so we can always refer to the original results
            clans = this.adapter.getAllPages();

        onAdapterSet();
    }

    /**
     * display the data once the adapter is set
     */
    private void onAdapterSet() {
        displayClanData(adapter.getCurrentPage());
    }

    /**
     * display the data passed to the fragment by the async worker
     *
     * @param page page
     */
    private void displayClanData(List<ClanTuple> page) {
        TableLayout table = activity.findViewById(R.id.table);

        if (adapter == null || table == null)
            return;

        activity.runOnUiThread(() -> {
            table.removeAllViews();
            View.inflate(activity, R.layout.layout_clan_table_header, table);
        });

        TextView pageNumber = activity.findViewById(R.id.page_number);

        activity.runOnUiThread(() -> pageNumber.setText(String.format(Locale.getDefault(),
                "Page %d of %d",
                adapter.getCurrentPageNumber(),
                adapter.getNumberOfPages())));

        activity.runOnUiThread(() -> {
            if (adapter.isFirstPage()) {
                activity.findViewById(R.id.previous).setVisibility(View.GONE);
            } else {
                activity.findViewById(R.id.previous).setVisibility(View.VISIBLE);
            }

            if (adapter.isLastPage()) {
                activity.findViewById(R.id.next).setVisibility(View.GONE);
            } else {
                activity.findViewById(R.id.next).setVisibility(View.VISIBLE);
            }
        });


        for (ClanTuple tuple : page) {

            JSONObject data = new JSONObject();

            try {
                data.put("platform", tuple.getPlatform());
                data.put("cid", tuple.getCid());
                data.put("tag",tuple.getTag());
                data.put("members", tuple.getMembers());
                data.put("wn8", tuple.getWn8());
                data.put("winRate", tuple.getWinRate());
                data.put("number", tuple.getNumber());
                data.put("even", tuple.getNumber() % 2 == 0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            activity.runOnUiThread(() -> table.addView(
                    new ClanTupleBuilder(data, activity).buildLayout()
            ));


        }
    }


    /**
     * configure clan table module
     * -- includes search bar and navigation buttons
     */
    private void configTableLayout() {
        View layoutContainer = View.inflate(activity, R.layout.layout_clan_table, view.findViewById(R.id.frame_layout));


        ImageView previous = layoutContainer.findViewById(R.id.previous);
        ImageView next = layoutContainer.findViewById(R.id.next);

        /*
        previous button
         */
        previous.setOnClickListener(V -> displayClanData(adapter.getPrevPage()));

        /*
        next button
         */
        next.setOnClickListener(V -> displayClanData(adapter.getNextPage()));


    }


    /**
     * configure switch to switch between platforms
     */
    private void configureSwitch() {
        Switch platformSwitch = view.findViewById(R.id.platform_switch);

        platformSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                buttonView.setText(activity.getText(R.string.xbox_clans));
                platform = "xbox";
            } else {
                buttonView.setText(activity.getText(R.string.playstation_clans));
                platform = "ps4";
            }

            clans = null;

            onRefresh();
        });
    }

    /**
     * configure the search field and button
     */
    private void configureSearch(){

        EditText input = view.findViewById(R.id.tag);
        ImageView button = view.findViewById(R.id.search);

        button.setOnClickListener(v ->{

            View view = activity.getCurrentFocus(); /* hide keyboard */
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }

            String tag = input.getText().toString();

            if (tag.isEmpty()) { //return everything
                setAdapter(new ClansFragment.Adapter(clans));
                return;
            }


            Toast.makeText(
                    activity,
                    "searching",
                    Toast.LENGTH_SHORT
            ).show();


            ArrayList<ClanTuple> searchResult = new ArrayList<>(); //return searched tag
            for( ClanTuple tuple: clans){
                if(tuple.getTag().toLowerCase().contains(tag.toLowerCase())){
                    searchResult.add(tuple);
                }
            }
            setAdapter(new ClansFragment.Adapter(searchResult));


        });
    }



    @Override
    public void onRefresh() {
        srl.setRefreshing(true);
        downloadClans();
    }


    /*=================================================================================*/
    /*=====================EVERYTHING BELOW IS ASYNCHRONOUS WORK=======================*/
    /*=================================================================================*/



    /** ASYNC!!!!!! *****************
     * download clan data and load it into view
     */
    private void downloadClans() {
        WorkerDownloadClans worker = new WorkerDownloadClans(platform, activity, this);
        worker.execute();



    }
}
