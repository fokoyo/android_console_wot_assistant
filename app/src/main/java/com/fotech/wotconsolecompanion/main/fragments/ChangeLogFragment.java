package com.fotech.wotconsolecompanion.main.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fotech.wotconsolecompanion.R;
import com.fotech.wotconsolecompanion.tools.data_cachers.LastUsedFragmentCache;
import com.fotech.wotconsolecompanion.utilities.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;


public class ChangeLogFragment extends AbstractFragment {

    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setToolbarTitle("Change Log");

        disableRefresh();

        activity = getActivity();

        return inflater.inflate(R.layout.fragment_change_log, container, false);
    }


    @Override
    public void onStart() {

        LastUsedFragmentCache.getInstance()
                .store(LastUsedFragmentCache.FragmentTypes.CHANGE_LOG_FRAGMENT + "");

        loadContent();

        super.onStart();
    }

    /**
     * load content into view
     */
    public void loadContent() {

        try {
            JSONObject stuff = new JSONObject(Util.loadJSONFromAsset(activity));


            displayStuff(
                    activity.findViewById(R.id.new_stuff),
                    stuff.getJSONArray("new_stuff"));


            displayStuff(activity.findViewById(R.id.next_stuff),
                    stuff.getJSONArray("next_stuff"));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * helper method
     *
     * @param container container for stuff
     * @param stuff     stuff (changes) to show
     * @throws Exception if an error occurs
     */
    private void displayStuff(ViewGroup container, JSONArray stuff) throws Exception {


        for (int i = 0; i < stuff.length(); i++) {
            View view = View.inflate(activity, R.layout.layout_change_log_text, null);

            TextView text = view.findViewById(R.id.text);
            text.setText(stuff.getString(i));

            TextView number = view.findViewById(R.id.number);
            number.setText(
                    String.format(Locale.getDefault(),
                            "%d--",
                            i + 1));

            container.addView(view);
        }


    }
}
